package com.pronetbeans.bamboo.buildlogexport;

/**
 * Utilities class.
 *
 * @author Adam Myatt
 */
public class Utils {

    public static Boolean getValueFromString(String value) {
        if (value == null) {
            value = "true";
        }
        return Boolean.parseBoolean(value);
    }

    public static String getLogBanner() {
        StringBuilder sb = new StringBuilder(1000);

        sb.append('\n');

        for (String s : Constants.banner) {
            sb.append(s);
            sb.append('\n');
        }

        return sb.toString();
    }
}
